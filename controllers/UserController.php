<?php

namespace app\controllers;

use Yii;

use yii\rest\ActiveController;
use app\models\User;
use app\models\Invitation;
use app\models\ClientTrainer;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                ['class' => \app\filters\auth\UidTokenAuth::className()]
            ],
            'except' => ['create','options']
        ];
        
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [],
            'actions' => [
                'incoming' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ],
            ],
        ];
        
        return $behaviors;
    }
    
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['create'], $actions['options']);
        
        return $actions;
    }
    
    protected function verbs()
    {     
        $verbs = parent::verbs();
        
        $verbs['index'] = ['POST','OPTIONS'];
        
        return $verbs;        
    }
    
    public function actionIndex(){
        return true;
    }
    
    public function actionOptions(){
        return true;
    }
    
    public function actionCreate()
    {        
        $data['User'] = Yii::$app->request->post();
        
        //Is invitation?
        $invitation = false;
        
        $user = new User();
        $user->setScenario('create');
        
        if(Yii::$app->request->post('inv_token')){
            $invitation_data = Invitation::findOne([
                'token' => Yii::$app->request->post('inv_token')
            ]);              
            
            if($invitation_data)
            {
                $user->type = User::TYPE['client'];
                $invitation = $invitation_data;
            }
            else{
                throw new \yii\web\NotAcceptableHttpException('Invalid invitation token');
            }
            
        }
        else{
            $user->type = User::TYPE['trainer'];
        }
        
        if($user->load($data) && $user->validate())
        {           
            $user->password_hash = Yii::$app->security->generatePasswordHash($user->password);
            $user->access_token = Yii::$app->security->generateRandomString();                

            if($user->save()){
                
                //Connect client with trainer
                if($invitation !== false)
                {
                    $connect = new ClientTrainer();
                    $connect->cid = $user->id;
                    $connect->tid = $invitation->uid;     
                    if($connect->save()){
                        //Set invitation status
                        $invitation->status = Invitation::STATUS['accepted'];
                        $invitation->save();
                    }
                }
                
                Yii::$app->response->statusCode = 201;
                return [
                    'message' => "User created!"
                ];
            }
            throw new \yii\web\HttpException(500, "Can't create user, please try again later.");
        }
        throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($user->errors));
    }

}