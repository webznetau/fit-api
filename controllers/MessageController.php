<?php

namespace app\controllers;

use Yii;

use yii\rest\ActiveController;
use app\models\Message;

class MessageController extends ActiveController
{
    public $modelClass = 'app\models\Message';
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                ['class' => \app\filters\auth\UidTokenAuth::className()]
            ]
        ];
        
        return $behaviors;
    }
    
    public function actions()
    {
        $actions = parent::actions();
        
        unset($actions);
        
        return $actions;
    }
    
    protected function verbs()
    {     
        return [
            'createmessage' => ['POST', 'OPTIONS'],
            'getconversations' => ['GET'],
            'getconversation' => ['GET'],
        ];
    }
    
    public function actionOptions(){
        return true;
    }
  
    
    /**
     * Send message to specified recipients
     * 
     * @return bool
     * @throws \yii\web\NotAcceptableHttpException if recipients are valid (users are not connected or recipient param isn't array etc.)
     * @throws \yii\web\HttpException when the message will not saved
     */
    public function actionCreatemessage(): bool
    {        
        $data['Message'] = Yii::$app->request->post();

        Message::convertSingleRecipientToRecipietsArray($data['Message']);
        
        $recipients = $data['Message']['to_id'];
        
        if (!Message::validateRecipients(Yii::$app->request->get('uid'), $recipients)) {
            throw new \yii\web\NotAcceptableHttpException("Invalid recipient");
        }
        
        foreach ($recipients as $to_id) {            
            $message = new Message();
            $message->time = time();
            $message->from_id = Yii::$app->request->get('uid');
            $message->conversation_id = Message::getConversationId(Yii::$app->request->get('uid'), $to_id);
            $data['Message']['to_id'] = (int)$to_id;
            
            if (!$message->load($data) || !$message->validate()) {
                throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($message->errors));
            }
            
            if (!$message->save()) {
                throw new \yii\web\HttpException(500, "Can't create message, please try again later.");
            }            
        }
        
        Yii::$app->response->statusCode = 201;
        
        return true;
    }

    /**
     * Get conversations list
     * 
     * @return array of conversations {@see Messages::getConversations()}
     */
    public function actionGetconversations(): array
    {
        return Message::getConversations((int)Yii::$app->request->get('uid'));
    }
    
    /**
     * Get conversation 
     * 
     * @param int $id
     * @param int $start offset for messages list (default 0)
     * @param int $limit number of messages (default 20)
     * @return array {@see Messages::getConversation()}
     * @throws \yii\web\NotFoundHttpException when the conversation with specified id doesn't exists for connected users
     */
    public function actionGetconversation(int $id, int $start = 0, int $limit = 20): array
    {
        $conversation = Message::getConversation($id, Yii::$app->request->get('uid'), $start, $limit);
                
        if (!$conversation) {
            throw new \yii\web\NotFoundHttpException("Can't find conversation.");
        }
        
        Message::markAsSeen($id, Yii::$app->request->get('uid'));
        
        return $conversation;
    }

}