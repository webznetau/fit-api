<?php

namespace app\controllers;

use Yii;

use yii\rest\ActiveController;
use app\models\Excersize;

class ExcersizeController extends ActiveController
{
    public $modelClass = 'app\models\Excersize';
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                ['class' => \app\filters\auth\UidTokenAuth::className()]
            ]
        ];
        
        return $behaviors;
    }
    
    public function actions()
    {
        $actions = parent::actions();
        unset($actions);
        
        return $actions;
    }
    
    protected function verbs()
    {     
        $verbs = parent::verbs();
        
        return $verbs;        
    }
    
    public function actionGetall(){
        return Excersize::find()
            ->where(['tid' => Yii::$app->request->get('uid')])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all(); 
    }
    
    public function actionOptions(){
        return true;
    }
    
    public function actionCreate()
    {        
        $data['Excersize'] = Yii::$app->request->post();
        
        $excersize = new Excersize();
        $excersize->tid = Yii::$app->request->get('uid');
                    
        if (!$excersize->load($data) || !$excersize->validate()) {
            throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($excersize->errors));
        }        
              
        if (!$excersize->save()) {
            Yii::$app->response->statusCode = 201;
            return true;
        }
        
        throw new \yii\web\HttpException(500, "Can't create excersize, please try again later.");
    }
    
    public function actionUpdate($id)
    {        
        $excersize = Excersize::findOne([
            'id' => (int)$id,
            'tid' => Yii::$app->request->get('uid')
        ]);               
        
        if (!$excersize) {                    
            throw new \yii\web\NotFoundHttpException("Can't find excersize");
        }     
        
        $data['Excersize'] = Yii::$app->request->post();

        if (!$excersize->load($data) || !$excersize->validate()) {
            throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($excersize->errors));
        }        

        if ($excersize->save()) {
            Yii::$app->response->statusCode = 201;
            return true;
        }
        throw new \yii\web\HttpException(500, "Can't update excersize, please try again later.");
    }
    
    public function actionGet($id)
    {
        $excersize = Excersize::find()
            -> select(['id','name','desc','h_links'])
            -> where([
                'id' => $id,
                'tid' => Yii::$app->request->get('uid')
            ])
            -> asArray()
            -> one();
        
        if (!$excersize) {
            throw new \yii\web\NotFoundHttpException("Can't find excersize");
        }
        
        return $excersize;
    }
    
    public function actionDelete($id)
    {
        $excersize = Excersize::findOne([
                'id' => $id,
                'tid' => Yii::$app->request->get('uid')
            ]);
        
        if (!$excersize) {
            throw new \yii\web\NotFoundHttpException("Can't find excersize");
        }        

        if ($excersize->delete()) {
            Yii::$app->response->statusCode = 201;
            return true;
        }
        throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($excersize->errors));
    }

}