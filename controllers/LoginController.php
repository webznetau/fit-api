<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Login;
use app\models\AccessToken;

class LoginController extends ActiveController
{
    public $modelClass = 'app\models\Login';
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [],
            'actions' => [
                'incoming' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ],
            ],
        ];
        
        return $behaviors;
        
    }

    public function actions()
    {
        $actions = parent::actions();
        
        unset($actions);

        return $actions;
    }
    
    public function actionIndex()
    {        
        $login = new Login();
        
        $login_data = [];
        $login_data['Login'] = Yii::$app->request->post();                    

        if($login -> load($login_data) && $login -> validate())
        {
            $user = $login -> getUser($login -> email);   

            if($user)
            {            
                if($user && Yii::$app->getSecurity()->validatePassword($login -> password, $user -> password_hash)){

                    $existing_token = AccessToken::getExisting($user->id);
                    
                    $token = $existing_token ? $existing_token : AccessToken::create($user->id);
                    
                    if($token){                    
                        return [
                            'uid' => $user -> id,
                            'token' => $token
                        ];
                    }
                     throw new \yii\web\HttpException(500, "Can't create access token, please try again later.");
                }
                throw new \yii\web\UnauthorizedHttpException("Invalid email or password");
            }
            throw new \yii\web\UnauthorizedHttpException("User doesn't exists");
        }
        
        throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($login -> errors));
    }
    
    protected function verbs()
    {     
        $verbs = parent::verbs();
        
        $verbs['index'] = ['POST','OPTIONS'];
        
        return $verbs;       
    }

}