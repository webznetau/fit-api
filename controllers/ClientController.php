<?php

namespace app\controllers;

use Yii;

use yii\rest\ActiveController;
use app\models\User;
use app\models\ClientTrainer;

class ClientController extends ActiveController
{
    public $modelClass = 'app\models\User';
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                ['class' => \app\filters\auth\UidTokenAuth::className()]
            ]
        ];
        
        return $behaviors;
    }
    
    public function actions()
    {
        $actions = parent::actions();
        unset($actions);
        
        return $actions;
    }
    
    protected function verbs()
    {     
        $verbs = parent::verbs();
        
        return $verbs;        
    }
    
    public function actionOptions(){
        return true;
    }
    
    public function actionIndex(){
        return User::find()
            ->select('{{%user}}.id, {{%user}}.first_name, {{%user}}.last_name, {{%user}}.email')
            ->leftJoin('{{%client_trainer}}', '{{%user}}.ID={{%client_trainer}}.cid')
            ->where([
                '{{%client_trainer}}.tid' => Yii::$app->request->get('uid')
            ])
            ->asArray()
            ->all();
    }
    
    public function actionGettrainers()
    {
        return User::find()
            ->select('{{%user}}.id, {{%user}}.first_name, {{%user}}.last_name, {{%user}}.email')
            ->leftJoin('{{%client_trainer}}', '{{%user}}.ID={{%client_trainer}}.tid')
            ->where([
                '{{%client_trainer}}.cid' => Yii::$app->request->get('uid')
            ])
            ->asArray()
            ->all();
    }
    

}