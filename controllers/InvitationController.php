<?php

namespace app\controllers;

use Yii;

use yii\rest\ActiveController;
use app\models\Invitation;
use app\models\User;
use app\models\ClientTrainer;

class InvitationController extends ActiveController
{
    public $modelClass = 'app\models\Invitation';
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                ['class' => \app\filters\auth\UidTokenAuth::className()]
            ],
            'except' => ['accept']
        ];
        
        return $behaviors;
    }
    
    public function actions()
    {
        $actions = parent::actions();
        unset($actions);
        
        return $actions;
    }
    
    protected function verbs()
    {     
        $verbs = parent::verbs();
        
        return $verbs;        
    }
    
    public function actionIndex(){
        return Invitation::find()
            ->where([
                'uid' => Yii::$app->request->get('uid')
            ])
            ->orderBy([
                'status' => SORT_ASC,
                'created_at' => SORT_DESC
            ])
            ->asArray()
            ->all();
    }
    
    public function actionOptions(){
        return true;
    }
    
    public function actionSendagain($id)
    {
        $invitation = Invitation::findOne([
            'id' => $id,
            'uid' => Yii::$app->request->get('uid')
        ]);
        
        if($invitation && ($invitation->status == Invitation::STATUS['pending'] || $invitation->status == Invitation::STATUS['canceled']))
        {
            $invitation -> status = Invitation::STATUS['pending'];
            $invitation -> created_at = time();
            
            $trainer = User::findOne($invitation->uid);
            
            if($invitation->save()){
                $this->sendInvitationEmail($invitation, $trainer);
                return true;
            }
            throw new \yii\web\HttpException(500, "Can't send invitation, please try again later.");
        }
        throw new \yii\web\NotFoundHttpException("This invitation doesn't exist.");
        
    }
    
    public function actionCancel($id)
    {
        $invitation = Invitation::findOne([
            'id' => $id,
            'uid' => Yii::$app->request->get('uid')
        ]);
        
        if($invitation && $invitation->status == Invitation::STATUS['pending'])
        {
            $invitation -> status = Invitation::STATUS['canceled'];
            
            if($invitation->save()){
                return true;
            }
            throw new \yii\web\HttpException(500, "Can't cancel invitation, please try again later.");
        }
        throw new \yii\web\NotFoundHttpException("This invitation doesn't exist or isn't pending.");
    }
    
    public function actionCreate()
    {        
        $data['Invitation'] = Yii::$app->request->post();
        
        $invitation = new Invitation();
        $invitation->token = Yii::$app->security->generateRandomString(32);     
        
        $trainer = User::findOne(Yii::$app->request->post('uid'));
                    
        if($invitation->load($data) && $invitation->validate())
        {            
            if(Invitation::findOne([
                'uid' => $invitation->uid,
                'email' => $invitation->email
            ])){
                throw new \yii\web\HttpException(409, "Client invitation was already sent");
            }
            
            $this->sendInvitationEmail($invitation, $trainer);
            
            if($invitation->save()){
                Yii::$app->response->statusCode = 201;
                return true;
            }
            throw new \yii\web\HttpException(500, "Can't create invitation, please try again later.");
        }
        throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($invitation->errors));
    }
    
    private function sendInvitationEmail($invitation, $trainer)
    {
        $accept_link = Yii::$app->params['api_url'].'/invitations/accept?token='.$invitation->token;
            
        $mail_body = <<<HTML
$trainer->first_name $trainer->last_name sent you an invitation to GET FIT APP. Click link above to accept:<br/>
                <a href="$accept_link">$accept_link</a>
HTML;
            
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['default_admin_email'])
                ->setTo($invitation->email)
                ->setSubject('GET FIT Trainer Invitation')
                ->SetTextBody($accept_link)
                ->setHtmlBody($mail_body)
                ->send();
    }
    
    public function actionAccept($token)
    {
        $invitation = Invitation::findOne([
            'token' => $token,
            'status' => Invitation::STATUS['pending'],
        ]);
        
        if($invitation){
            //Check if client exists
            $user = User::findOne([
                'email' => $invitation->email,
                'type' => User::TYPE['client']
            ]);
            
            if($user)
            {
                //Create connection client <=> trainer
                $connection = new ClientTrainer();
                $connection->tid = $invitation->uid;
                $connection->cid = $user->id;

                if($connection -> save()){      
                    //Set invitation as accepted
                    $invitation->status = Invitation::STATUS['accepted'];
                    $invitation->save();
                    
                    return $this->redirect(Yii::$app->params['desktop_app_url'].'/user/invitation-accepted');
                }
                else{
                    return $this->invitationFailRedirect();
                }
            }
            
            return $this->redirect(Yii::$app->params['desktop_app_url'].'/user/guest/signup?inv_token='.$invitation->token.'&email='.$invitation->email);
        }
        
        return $this->invitationFailRedirect();
    }
    
    private function invitationFailRedirect()
    {
        return $this->redirect(Yii::$app->params['desktop_app_url'].'/user/invitation-fail');
    }

}