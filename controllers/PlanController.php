<?php

namespace app\controllers;

use Yii;

use yii\rest\ActiveController;
use app\models\Plan;

class PlanController extends ActiveController
{
    public $modelClass = 'app\models\Plan';
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\CompositeAuth::className(),
            'authMethods' => [
                ['class' => \app\filters\auth\UidTokenAuth::className()]
            ]
        ];
        
        return $behaviors;
    }
    
    public function actions()
    {
        $actions = parent::actions();
        unset($actions);
        
        return $actions;
    }
    
    protected function verbs()
    {     
        $verbs = parent::verbs();
        
        return $verbs;        
    }
    
    public function actionGetall($cid = 0)
    {        
        return Plan::find()
            ->where([
                'tid' => Yii::$app->request->get('uid'),
                'cid' => (int)$cid
            ])
            ->orderBy([
                'name' => SORT_ASC
            ])
            ->asArray()
            ->all(); 
    }
    
    public function actionOptions(){
        return true;
    }
    
    public function actionCreate($cid = 0)
    {        
        $data['Plan'] = Yii::$app->request->post();
        
        $plan = new Plan();
        $plan->tid = Yii::$app->request->get('uid');
        $plan->cid = $cid;
        
        $data['Plan']['groups'] = json_encode($data['Plan']['groups']);
                    
        if($plan->load($data) && $plan->validate())
        {            
            if($plan->save()){
                Yii::$app->response->statusCode = 201;
                return true;
            }
            throw new \yii\web\HttpException(500, "Can't create plan, please try again later.");
        }
        throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($plan->errors));
    }
    
    public function actionUpdate($id)
    {        
        $plan = Plan::findOne([
            'id' => (int)$id,
            'tid' => Yii::$app->request->get('uid')
        ]);               
        
        if(!$plan) {                    
            throw new \yii\web\NotFoundHttpException("Can't find plan");
        }     
        
        $data['Plan'] = Yii::$app->request->post();
        $data['Plan']['groups'] = json_encode($data['Plan']['groups']);

        if($plan->load($data) && $plan->validate())
        {
            if($plan->save()){
                Yii::$app->response->statusCode = 201;
                return true;
            }
            throw new \yii\web\HttpException(500, "Can't update plan, please try again later.");
        }
        throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($plan->errors));
    }
    
    public function actionGet($id)
    {
        $plan = Plan::find()
//            -> select(['id','name','desc','h_links'])
            -> where([
                'id' => $id,
                'tid' => Yii::$app->request->get('uid')
            ])
            -> asArray()
            -> one();
        
        if($plan){
            return $plan;
        }
        
        throw new \yii\web\NotFoundHttpException("Can't find plan");
    }
    
    public function actionDelete($id)
    {
        $plan = Plan::findOne([
                'id' => $id,
                'tid' => Yii::$app->request->get('uid')
            ]);
        
        if($plan){
            if($plan->delete()){
                Yii::$app->response->statusCode = 201;
                return true;
            }
            throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($plan->errors));
        }
        
        throw new \yii\web\NotFoundHttpException("Can't find plan");
    }
    
    public function actionCopy($id, $cid = 0)
    {
        $plan = Plan::find()
            -> where([
                'id' => $id,
                'tid' => Yii::$app->request->get('uid')
            ])
            -> one();
        
        if($plan)
        {            
            $plan->id = null;
            $plan->isNewRecord = true;
            $plan->name = $plan->name.' - COPY';
            $plan->cid = $cid;

            if($plan->save()){
                Yii::$app->response->statusCode = 201;
                return true;
            }
            throw new \yii\web\NotAcceptableHttpException(\app\helpers\StringHelper::modelErrorsToString($plan->errors));
      
        }
        
        throw new \yii\web\NotFoundHttpException("Can't find plan");
    }

}