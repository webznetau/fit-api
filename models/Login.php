<?php

namespace app\models;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Login extends \yii\db\ActiveRecord
{
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['password','email'], 'string']
        ];
    }

    
    public static function tableName()
    {
        parent::tableName();
        
        return '{{%user}}';
    }
    
    public function validatePassword($attribute, $params)
    {
        if(!$this->hasErrors())
        {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)){
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }
    
    public function getUser($email)
    {
        return self::findOne(['email' => $email]);
    }
}
