<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


/**
 * User model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $password;
    public $password_repeat;
    
    const STATUS = [
        'removed' => -1,
        'inactive' => 0,
        'active' => 10
    ];
    
    const SCENARIO = [
        'create' => 'create',
        'update'=> 'update'
    ];
    
    const TYPE = [
        'trainer' => 50,
        'client' => 20
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email','first_name','last_name', 'password_repeat'], 'required'],
            [['password'], 'string', 'min' => '8', 'skipOnEmpty' => true, 'on' => self::SCENARIO['update']],
            [['password'], 'required', 'on' => self::SCENARIO['create'] ],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=> "Passwords don't match" ],
            [['email'], 'email'],
            [['email'], 'unique'],
            ['password', 'required', 'on' => 'create', 'message' => 'Password cannot be blank.'],
            [['first_name','last_name'], 'string'],
            [['type'], 'integer'],
            ['status', 'default', 'value' => self::STATUS['active']],
            ['status', 'in', 'range' => [self::STATUS['active'], self::STATUS['inactive'], self::STATUS['removed']]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS['active']]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {        
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS['active']]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS['active'],
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public static function findByUidAndAccessToken( $uid, $token )
    {
        $query = new \yii\db\Query;
        $query	-> select(['{{%user}}.id AS id', '{{%access_token}}.uid' ,'{{%access_token}}.token as token'])  
                -> from('{{%user}}')
                -> rightJoin('{{%access_token}}', '{{%access_token}}.uid = {{%user}}.id')
                -> where([
                    '{{%access_token}}.token'=> $token, 
                    '{{%user}}.id' => $uid,
                    '{{%access_token}}.user_agent' => Yii::$app -> request -> userAgent
                ])
                -> andWhere(['>', '{{%access_token}}.expiry', time()]);
        
        $command = $query -> createCommand();
        return $command -> queryOne() ? true : false;
    }
   
}
