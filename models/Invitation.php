<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class Invitation extends \yii\db\ActiveRecord
{    
    const STATUS = [
        'pending' => 0,
        'accepted' => 1,
        'canceled' => -1
    ];
    
    public function rules()
    {
        return [
            [['token', 'uid', 'email'], 'required'],
            [['token'], 'string'],
            ['email', 'email'],
            [['status','uid'], 'integer']
        ];
    }
    
    public static function tableName()
    {        
        return '{{%invitation}}';
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
