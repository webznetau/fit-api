<?php

namespace app\models;

use Yii;

class AccessToken extends \yii\db\ActiveRecord
{
    const TOKEN_EXPIRY = 604800;
    
    public function rules()
    {
        return [
            [['uid', 'token', 'user_agent', 'expiry'], 'required'],
            [['token','user_agent'], 'string'],
            ['expiry', 'integer']
        ];
    }
    
    public static function tableName()
    {
        parent::tableName();
        
        return '{{%access_token}}';
    }
    
    public static function getExisting($uid)
    {
        $user_agent = Yii::$app->request->userAgent;
        
        $find = self::find()
                ->where([
                    'uid' => $uid,
                    'user_agent' => $user_agent,
                ])
                ->andWhere(['>', 'expiry', (time()+86400)])
                ->one();
        
        if($find){
            return $find->token;
        }
        return false;
    }
    
    public static function create($uid)
    {
        $token = new AccessToken();
        $token->uid = $uid;
        $token->token = Yii::$app->security->generateRandomString();
        $token->user_agent = Yii::$app->request->userAgent;
        $token->expiry = time() + self::TOKEN_EXPIRY;
        
        if($token->save()){
            return $token->token;
        }
        return false;
    }
}
