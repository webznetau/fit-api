<?php

namespace app\models;

use Yii;

class ClientTrainer extends \yii\db\ActiveRecord
{    
    public function rules()
    {
        return [
            [['cid','tid'], 'required'],
            [['cid','tid'], 'integer']
        ];
    }
    
    public static function tableName()
    {        
        return '{{%client_trainer}}';
    }
}
