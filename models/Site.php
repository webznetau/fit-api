<?php

namespace app\models;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Login extends \yii\db\ActiveRecord
{
    public $username;
    public $password;

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['password','username'], 'string'],
        ];
    }

    
    public static function tableName()
    {
        parent::tableName();
        
        return '{{%user}}';
    }
    
    public function validatePassword($attribute, $params)
    {
        if(!$this->hasErrors())
        {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)){
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }
    
    public function getUser($username)
    {
        return self::findOne(['username' => $username]);
    }
}
