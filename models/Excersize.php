<?php

namespace app\models;

use Yii;

class Excersize extends \yii\db\ActiveRecord
{ 
    
    public function rules()
    {
        return [
            [['name', 'tid'], 'required'],
            [['name','desc','h_links'], 'string'],
            [['tid'], 'integer']
        ];
    }
    
    public static function tableName()
    {        
        return '{{%excersize}}';
    }
    
}
