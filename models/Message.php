<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\ClientTrainer;

class Message extends \yii\db\ActiveRecord
{ 
    
    public function rules()
    {
        return [
            [['content', 'from_id', 'to_id', 'time', 'conversation_id'], 'required'],
            [['content'], 'string'],
            [['from_id', 'to_id', 'time', 'seen_time', 'conversation_id'], 'integer']
        ];
    }
    
    
    public static function tableName()
    {        
        return '{{%message}}';
    }
    
    /**
     * Get conversations for concrete user
     * 
     * @param int $uid user ids
     * @return array of messages [id, coversation_id, to_id, from_id, content, time, seen_time, from_name, to name]
     */
    public static function getConversations(int $uid): array
    {        
        $connection = Yii::$app->getDb();
        
        $command =  $connection->createCommand("SELECT messages.*, "
                . "CONCAT_WS(' ', user1.first_name, user1.last_name) as from_name, "
                . "CONCAT_WS(' ', user2.first_name, user2.last_name) as to_name "
            . "FROM " . self::tableName() . " messages "            
            . "LEFT JOIN " . User::tableName() . " user1 ON user1.id = messages.from_id "
            . "LEFT JOIN " . User::tableName() . " user2 ON user2.id = messages.to_id "
            . "JOIN (SELECT id, from_id, to_id, time, seen_time, content, conversation_id, MAX(time) max_time FROM " . self::tableName() . " GROUP BY conversation_id) sub "
            . "ON sub.conversation_id = messages.conversation_id
                AND sub.max_time = messages.time "
            . "WHERE messages.from_id = '" . (int)$uid . "' "
                . "OR messages.to_id = '" . (int)$uid . "' "
            . "GROUP BY conversation_id "
            . "ORDER BY messages.time DESC");

        return $command->queryAll();
    }
    
    /**
     * Validate recipients. Check recipients param format & sender is connected with each recipient
     * 
     * @param int $from_id sender id
     * @param array $recipients recipients ids
     * @return bool
     */
    public static function validateRecipients(int $from_id, array $recipients): bool
    {        
        if (!self::validateRecipientsArray($recipients)) {
            return false;
        }
        
        if (count($recipients) == 1) {
            $check = ClientTrainer::find()
                ->where(['tid' => (int)$from_id, 'cid' => $recipients[0]])
                ->orWhere(['cid' => (int)$from_id, 'tid' => $recipients[0]])
                ->one(); 
            
            return $check ? true : false;
        }
        
        foreach ($recipients as $id) {            
            $check = ClientTrainer::find()
                ->where(['tid' => (int)$from_id, 'cid' => (int)$id])
                ->orWhere(['cid' => (int)$from_id, 'tid' => (int)$id])
                ->one(); 
            
            if (!$check) {
                return false;
            }
        }
        
        return true;
    }
    
    private static function validateRecipientsArray(array $recipients): bool
    {
        return is_array($recipients) && count($recipients) >= 1 ? true : false;
    }
    
    /**
     * Getting conversation id
     * 
     * @param int $from_id sender id
     * @param int $to_id recipient id
     * @return int conversation id
     */
    public static function getConversationId(int $from_id, int $to_id): int
    {
        $find_conversation = self::find('conversation_id')
            ->where(['to_id' => $to_id, 'from_id' => $from_id])
            ->orWhere(['to_id' => $from_id, 'from_id' => $to_id])
            ->one();
        
        if ($find_conversation) {
            return $find_conversation->conversation_id;
        }
        
        $find_last_conversation = Message::find('conversation_id')
            ->orderBy('conversation_id DESC')
            ->one();

        if ($find_last_conversation) {
            return $find_last_conversation->conversation_id + 1;
        } 
        
        return 1;        
    }
    
    /**
     * Get conversation messages
     * 
     * @param int $id conversation id
     * @param int $uid user id
     * @param int $start offset for messages list
     * @param int $limit number of messages
     * @return array of messages [id, conversation_id, to_id, from_id, content, time, seen_time]
     */
    public static function getConversation(int $id, int $uid, int $start, int $limit): array
    {
//        return self::find()
//            ->where(['conversation_id' => (int)$id])
//            ->andWhere(['from_id' => $uid])
//            ->orWhere(['to_id' => $uid])
//            ->offset((int)$start)
//            ->limit((int)$limit)
//            ->orderBy('time DESC') 
//            ->all();
        $connection = Yii::$app->getDb();
        
        $command =  $connection->createCommand("SELECT messages.*, "
                . "CONCAT_WS(' ', user1.first_name, user1.last_name) as from_name, "
                . "CONCAT_WS(' ', user2.first_name, user2.last_name) as to_name "
            . "FROM " . self::tableName() . " messages "
            . "LEFT JOIN " . User::tableName() . " user1 ON user1.id = messages.from_id "
            . "LEFT JOIN " . User::tableName() . " user2 ON user2.id = messages.to_id "
            . "WHERE (messages.from_id = '" . (int)$uid . "' "
                . "OR messages.to_id = '" . (int)$uid . "')"
                . "AND messages.conversation_id = " . (int)$id . " "
            . "ORDER BY messages.time DESC "
            . "LIMIT " . $start . "," . $limit . "");
        
        return $command->queryAll();
    }
    
    /**
     * Mark messages sent to specified user as seen (set timestamp for them)
     * 
     * @param int $conversation_id conversation id
     * @param int $uid user id
     * @return void
     */
    public static function markAsSeen(int $conversation_id, int $uid): void
    {
        self::updateAll(['seen_time' => time()], ['and', 
            ['=','to_id', $uid],
            ['=', 'conversation_id', (int)$conversation_id]
        ]);
    }
    
    /**
     * 
     * @param array $message
     * @return void
     */
    public static function convertSingleRecipientToRecipietsArray(array &$message): void
    {
        if (!is_array($message['to_id'])) {
            $_to_id = $message['to_id'];            
            $message['to_id'] = [];
            $message['to_id'][] = $_to_id;
        }
    }
}
