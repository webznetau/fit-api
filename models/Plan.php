<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class Plan extends \yii\db\ActiveRecord
{ 
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    
    public function rules()
    {
        return [
            [['name', 'tid'], 'required'],
            [['name','desc','groups'], 'string'],
            [['tid','cid'], 'integer']
        ];
    }
    
    public static function tableName()
    {        
        return '{{%plan}}';
    }
    
}
