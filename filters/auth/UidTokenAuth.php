<?php

namespace app\filters\auth;
use yii\filters\auth\AuthMethod;
use app\models\User;

class UidTokenAuth extends AuthMethod
{
    public $tokenParam = 'token';
    public $uidParam = 'uid';

    public function authenticate($user, $request, $response)
    {
        $token = $request -> get($this->tokenParam);
        $uid = $request -> get($this->uidParam);
        
        if (is_string($token)) {
            
            if($token !== NULL && $uid !== NULL)
            {            
                $identity = User::findByUidAndAccessToken($uid, $token);

                if ($identity !== false) {
                    return $identity;
                }
            }
        }
        if ($token !== null) {
            $this->handleFailure($response);
        }

        return null;
    }
}
