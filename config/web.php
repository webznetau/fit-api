<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]    
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
            },
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableSession' => false,
            'loginUrl' => null
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => ['site', 'user', 'login'],
                ],       
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'client',
                    'extraPatterns' => [
                        'GET' => 'index',
                        'GET trainers' => 'gettrainers'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'invitation',
                    'extraPatterns' => [
                        'GET accept' => 'accept',
                        'POST send-again' => 'sendagain',
                        'OPTIONS send-again' => 'options',
                        'POST cancel' => 'cancel',
                        'OPTIONS cancel' => 'options'
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'excersize',
                    'extraPatterns' => [
                        'GET ' => 'getall',
                        'GET {id}' => 'get'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'plan',
                    'extraPatterns' => [
                        'GET ' => 'getall',
                        'GET {id}' => 'get',
                        'POST copy/{id}' => 'copy',
                        'OPTIONS copy/{id}' => 'options',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'message',
                    'extraPatterns' => [
                        'POST create' => 'createmessage',
                        'OPTIONS create' => 'options',
                        'GET conversations' => 'getconversations',
                        'GET conversations/{id}' => 'getconversation',
                    ]
                ]
            ],
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
