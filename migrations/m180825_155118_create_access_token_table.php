<?php

use yii\db\Migration;

/**
 * Handles the creation of table `access_token`.
 */
class m180825_155118_create_access_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%access_token}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->integer(10)->notNull(),
            'token' => $this->string(32)->notNull(),
            'user_agent' => $this->text()->notNull(),
            'expiry' => $this->integer(10)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('access_token');
    }
}
