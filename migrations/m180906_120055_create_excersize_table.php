<?php

use yii\db\Migration;

/**
 * Handles the creation of table `excersize`.
 */
class m180906_120055_create_excersize_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%excersize}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'desc' => $this->text(),
            'h_links' => $this->text(),
            'tid' => $this->integer(10)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('excersize');
    }
}
