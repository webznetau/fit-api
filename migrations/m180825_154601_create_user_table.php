<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180825_154601_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(250)->notNull(),
            'first_name' => $this->string(250)->notNull(),
            'last_name' => $this->string(250)->notNull(),
            'access_token' => $this->string(32)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'password_reset_token' => $this->string(255),
            'status' => $this->tinyInteger(2)->notNull(),
            'type' => $this->integer(2)->notNull(),
            'created_at' => $this->integer(10)->notNull(),
            'updated_at' => $this->integer(10)->notNull()            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
