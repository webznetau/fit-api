<?php

use yii\db\Migration;

/**
 * Handles the creation of table `invitation`.
 */
class m180831_091818_create_invitation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%invitation}}', [
            'id' => $this->primaryKey(),
            'token' => $this->string(32)->notNull(),
            'uid' => $this->integer(10)->notNull(),
            'email' => $this->string(100)->notNull(),
            'created_at' => $this->integer(10)->notNull(),
            'updated_at' => $this->integer(10)->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('invitation');
    }
}
