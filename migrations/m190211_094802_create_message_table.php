<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message`.
 */
class m190211_094802_create_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'conversation_id' => $this->integer(10)->notNull(),
            'to_id' => $this->integer(10)->notNull(),
            'from_id' => $this->integer(10)->notNull(),
            'content' => $this->text()->notNull(),
            'time' => $this->integer(10)->notNull(),
            'seen_time' => $this->integer(10)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('message');
    }
}
