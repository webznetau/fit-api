<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client_trainer`.
 */
class m180831_104424_create_client_trainer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client_trainer}}', [
            'id' => $this->primaryKey(),
            'cid' => $this->integer(10)->notNull(),
            'tid' => $this->integer(10)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('client_trainer');
    }
}
