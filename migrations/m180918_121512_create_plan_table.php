<?php

use yii\db\Migration;

/**
 * Handles the creation of table `plan`.
 */
class m180918_121512_create_plan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%plan}}', [
            'id' => $this->primaryKey(),
            'tid' => $this->integer(10)->notNull(),
            'cid' => $this->integer(10)->notNull()->defaultValue(0),
            'name' => $this->string()->notNull(),
            'desc' => $this->text(),
            'groups' => $this->text(),
            'created_at' => $this->integer(10)->notNull(),
            'updated_at' => $this->integer(10)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('plan');
    }
}
