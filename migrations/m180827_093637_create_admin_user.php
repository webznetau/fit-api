<?php

use yii\db\Migration;
use app\models\User;

/**
 * Class m180827_093637_create_admin_user
 */
class m180827_093637_create_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'email' => Yii::$app->params['default_admin_email'],
            'first_name' => 'Apollo',
            'last_name' => 'Creed',
            'access_token' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash(Yii::$app->params['default_admin_password']),
            'password_reset_token' => NULL,
            'type' => User::TYPE['trainer'],
            'status' => User::STATUS['active'],
            'created_at' => time(),
            'updated_at' => time()            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', [
            'id' => 1
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180827_093637_create_admin_user cannot be reverted.\n";

        return false;
    }
    */
}
