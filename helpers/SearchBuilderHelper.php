<?php
namespace app\helpers;

class SearchBuilderHelper
{    
    /**
     * 
     * @param array $filters = [
     *      param1 => [
     *          '>' => 123,
     *          '<' <= 311
     *      ] 
     * }
     * @param type $av_params = [
     *      param1 => [operator1, operator2],
     *      param2,
     *      param3
     * ]
     */
    private static function buildAndWhere( $filters, $av_params )
    {
        $andWhere = [];
        
        if( count( $filters ) == 0 || !is_array( $filters ) ) {
            return null;
        }
        
        foreach( $filters as $param => $data ) { 

            foreach( $data as $operator => $_value ) {
                
                if( array_key_exists( $param, $av_params ) && ( $av_params[ $param ] == '*' || in_array( $operator, $av_params[ $param ] ) ) ) {
                    
                    switch( $operator ) {
                        
                        case '>':
                        case 'gt':
                            $andWhere[] = ['>', $param, $_value ];
                            break;
                        case '<':
                        case 'lt':
                            $andWhere[] = ['<', $param, $_value ];
                            break;
                        case '>=':
                        case 'gte':
                            $andWhere[] = ['>=', $param, $_value ];
                            break;
                        case '<=':
                        case 'lte':
                            $andWhere[] = ['<=', $param, $_value ];
                            break;
                        case '=':
                        case 'eq':
                            $andWhere[] = ['=', $param, $_value ];
                            break;
                        case 'between':
                            $explode = explode('-', $_value);
                            $andWhere[] = ['>=', $param, $explode[0] ];
                            $andWhere[] = ['<=', $param, $explode[1] ];
                            break;
                        
                    }
                    
                }
                
            }            
            
        }

        return $andWhere;
    }
    
    static function extend( $model, $filters, $av_params ) {
        
        
        $andWhere = self::buildAndWhere( $filters, $av_params );
        
        if( $andWhere ) {
            
            foreach( $andWhere as $_andWhere ) {
                
                $model = $model -> andWhere( $_andWhere );
                
            }
            
        }
        
        return $model;
        
    }
    
}