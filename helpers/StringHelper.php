<?php
namespace app\helpers;

class StringHelper extends \yii\helpers\StringHelper
{    
    public static function modelErrorsToString($errors, $break_line = "<br/>", $prepend = "")
    {
        $string = "";
        $c = count($errors);
        if($c > 0){
            $i = 1; 
            foreach($errors as $err)
            {
                foreach($err as $msg)
                {
                    if($msg !== ''){
                        $string .= $prepend.$msg;

                        if($c > 1 && $i < $c){
                            $string .= $break_line;
                        }
                    }
                    $i++;
                }                                
            }
        }
        return $string;
    }
    
    public static function generateSpacer($depth, $spacer = " ")
    {
        $ret = "";
        for($i=0; $i<$depth; $i++)
        {
            $ret .= $spacer;
        }

        
        return $ret;
    }
    
    public static function getBetween($open_char, $close_char, $string)
    {
        $matches = [];
        $regex = "/$open_char([a-zA-Z0-9_]*)$close_char/";
        preg_match_all($regex, $string, $matches);
        return count($matches[1]) > 0 ? $matches[1] : null;
    }
    
}